from random import randint

from utilities import load_blocks, play_sound


class Grid:
    GRAY_BLOCK = 7  # gray block is used in multiplayer
    """ Score for clearing 1, 2, 3 or 4 lines in one move. """
    COMBO_SCORES = (100, 250, 450, 800)

    def __init__(self, rows, columns, block_size, background):
        """
        :param (int) columns: Number of columns.
        :param (int) rows: Number of rows.
        :param (int) block_size:
        :param (Surface) background: Grid background image.
        """
        self.lines = 0
        self.score = 0
        self.game_over = False

        self.block_size = block_size
        self.area_width = columns * self.block_size
        self.area_height = rows * self.block_size

        self.columns = columns
        self.rows = rows
        # rows x columns grid
        self.grid = [[-1 for i in range(self.columns)]
                     for j in range(self.rows)]
        self.background = background
        self.background.set_alpha(210)

        self.blocks = load_blocks('blocks', block_size)
        self.blocks_assist = load_blocks('blocks_assist', block_size)

    def get_size(self):
        return self.area_width, self.area_height

    def get_block_size(self):
        return self.block_size

    def get_lines(self):
        return self.lines

    def get_score(self):
        return self.score

    """
    :param columns: This parameter is used in coop play to find the boundaries
     of each player, because they share the same grid.
    """

    def get_center_coord(self, columns=None):
        """
        Calculate the center coord of the grid's columns.
        :param (tuple) columns: (start, end) start <= index < end
        :return: A tuple (x, y) of the center coordinates.
        """
        if columns is None:
            columns = (0, self.columns)

        start, end = columns
        return (start + (end - start) // 2) * self.block_size, self.block_size

    def is_move_valid(self, tetromino, columns=None):
        """
        Check if the tetromino is in a valid position. In order to be in a
        valid position it must not be out of bounds and not overlap other
        tetrominoes.
        :param (Tetromino) tetromino:
        :param (tuple) columns: (start, end) start <= index < end
        :return: True if tetromino is in a valid position.
        """
        return not self.is_out_of_bounds(tetromino, columns) \
               and not self.overlap(tetromino)

    def is_out_of_bounds(self, tetromino, columns=None):
        """
        Check if all blocks of the tetromino are inside the bounds.
        :param (Tetromino) tetromino:
        :param (tuple) columns: (start, end) start <= index < end
        :return: True if tetromino is out of bounds.
        """
        if columns is None:
            x_min = 0
            x_max = (self.columns - 1) * self.block_size
        else:
            x_min = columns[0] * self.block_size
            x_max = (columns[1] - 1) * self.block_size
        for x, y in tetromino.get_coords():
            if not x_min <= x <= x_max:
                return True
        return False

    def is_game_over(self):
        return self.game_over

    def overlap(self, tetromino):
        """
        :param (Tetromino) tetromino:
        :return: True if blocks overlap or passed the bottom of the grid.
        """
        indexes_list = self._convert_coords(tetromino.get_coords())
        for row_index, column_index in indexes_list:
            if row_index >= self.rows or row_index < 0 or\
                    self.grid[row_index][column_index] >= 0:
                return True
        return False

    def insert_tetromino(self, tetromino):
        """
        Insert a new tetromino in the grid, remove full rows and update the
        score. For each block of the tetromino find the index in the grid and
        set TetrominoType value as the value of the cell. Tetromino type and
        block sprite index coincide.
        :param (Tetromino) tetromino:
        """
        lines_before = self.lines
        indexes_list = self._convert_coords(tetromino.get_coords())
        for row_index, column_index in indexes_list:
            if row_index >= 0:
                self.grid[row_index][column_index] = tetromino.get_type().value

                # There is a block at the top row.
                if row_index == 0:
                    self.game_over = True
                    continue

                # Search for full rows.
                full_row = True
                for j in range(self.columns):
                    if self.grid[row_index][j] == -1:  # cell is empty
                        full_row = False
                        break

                # Delete the row if it is full.
                if full_row:
                    self.lines += 1
                    del self.grid[row_index]
                    # Insert a new line at the top of the grid
                    self.grid.insert(0, [-1 for _ in range(self.columns)])

        # Update the score.
        lines_cleared = self.lines - lines_before
        if lines_cleared:
            self.score += Grid.COMBO_SCORES[lines_cleared - 1]
            play_sound('clear')

    def insert_blocks(self, lines):
        """
        Insert gray block lines at the bottom of the grid and remove the same
        number of lines at the top of the grid.
        :param (int) lines: Number of lines.
        """
        # The lines have a random empty column.
        empty_column_index = randint(0, self.columns - 1)
        for i in range(lines):
            new_line = [self.GRAY_BLOCK for _ in range(self.columns)]
            new_line[empty_column_index] = -1
            self.grid.pop(0)
            self.grid.append(new_line)

    def render_assist_tetromino(self, surface, tetromino):
        assist_coords = self._assist_tetromino(tetromino)
        for coord in assist_coords:
            surface.blit(self.blocks_assist[tetromino.get_type().value], coord)

    def render(self, surface):
        """
        Render background and all blocks.
        :param (Surface) surface:
        """
        surface.blit(self.background, (0, 0))
        for i in range(self.rows):
            for j in range(self.columns):
                if self.grid[i][j] >= 0:  # if cell isn't empty (empty -> -1)
                    # cell value is tetromino type
                    tetromino_type = self.grid[i][j]
                    coord_x = j * self.block_size
                    coord_y = i * self.block_size
                    surface.blit(self.blocks[tetromino_type],
                                 (coord_x, coord_y))

    def _convert_coords(self, coords):
        """
        Convert coordinates to grid indexes.
        :param (tuple | list) coords: List of coordinates (x, y).
        :return: List of indexes (row_index, column_index).
        """
        indexes_list = []
        for coord in coords:
            column_index = coord[0] // self.block_size
            row_index = coord[1] // self.block_size
            indexes_list.append((row_index, column_index))
        return indexes_list

    def _convert_indexes(self, indexes):
        """
        Convert grid indexes to coordinates.
        :param (tuple | list) indexes: List of indexes (row_index,
        column_index).
        :return: List of coordinates (x, y).
        """
        coords_list = []
        for index in indexes:
            x = index[1] * self.block_size
            y = index[0] * self.block_size
            coords_list.append((x, y))
        return coords_list

    def _assist_tetromino(self, tetromino):
        """
        Calculate the coordinates of the tetromino when it reaches the
        bottom of the grid.
        :param (Tetromino) tetromino:
        :return: List of assist block coordinates.
        """
        tetromino_coords = tetromino.get_coords()
        indexes_list = self._convert_coords(tetromino_coords)
        bottom = False
        overlap = False

        # Tetromino reached bottom.
        for row_index, column_index in indexes_list:
            if row_index >= self.rows - 1:
                return tetromino_coords

        while not bottom and not overlap:
            # For every block.
            for i in range(len(indexes_list)):
                row_index, column_index = indexes_list[i]
                indexes_list[i] = (row_index + 1, column_index)
                # Check next row.
                if self.grid[row_index + 1][column_index] >= 0:
                    overlap = True
                elif row_index + 1 >= self.rows - 1:
                    bottom = True

        if overlap:
            # Move blocks one row up.
            for i, (row_index, column_index) in enumerate(indexes_list):
                indexes_list[i] = (row_index - 1, column_index)

        return self._convert_indexes(indexes_list)

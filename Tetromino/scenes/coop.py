from pygame import display

from config import ANIMATED_BACKGROUND
from grids.coop import CoopGrid
from hud import HUD
from parallax import ParallaxBackground
from player import Player
from scenes.base import Scene
from utilities import ARROW_KEYS, LETTER_KEYS


class Coop(Scene):
    SCREEN_WIDTH = 900
    SCREEN_HEIGHT = 720
    BLOCK_SIZE = 30

    def __init__(self, game):
        super().__init__(game)
        self.resize()

        self.grid = CoopGrid(self.BLOCK_SIZE)
        self.left_player = Player(self.grid, LETTER_KEYS, (0, 10))
        self.right_player = Player(self.grid, ARROW_KEYS, (10, 20))
        self.right_player.set_next_tetromino_position((
            Player.NEXT_TETROMINO_COORD[0],
            Player.NEXT_TETROMINO_COORD[1] + self.BLOCK_SIZE * 3))

        self.background = ParallaxBackground('background',
                                             (self.SCREEN_WIDTH,
                                              self.SCREEN_HEIGHT))

        self.hud = HUD()
        self.hud.add_item(label='message',
                          position=(self.SCREEN_WIDTH // 2 + 80,
                                    self.SCREEN_HEIGHT // 2 - 50),
                          size=70,
                          color=(255, 255, 255),
                          value='GAME OVER',
                          visible=False,
                          center_x=True)

    def resize(self):
        display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))

    def tick(self, dt):
        if self.left_player.is_game_over():
            self.game.remove_scene()
            return

        self.background.tick(dt)
        self.left_player.tick(dt)
        self.right_player.tick(dt)

    def event(self, event):
        self.left_player.event(event)
        self.right_player.event(event)

    def render(self, surface):
        if self.left_player.is_game_over():
            self.hud.toggle_visibility('message')
            self.game.set_delay(2500)

        self.background.render(surface, ANIMATED_BACKGROUND)
        self.left_player.render(surface)
        self.right_player.render(surface, hud=False, grid=False)
        self.hud.render(surface)

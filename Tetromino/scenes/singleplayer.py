from pygame import display

from config import ANIMATED_BACKGROUND
from grids.classic import ClassicGrid
from hud import HUD
from parallax import ParallaxBackground
from player import Player
from scenes.base import Scene
from utilities import ARROW_KEYS


class Singleplayer(Scene):
    SCREEN_WIDTH = 640
    SCREEN_HEIGHT = 720
    BLOCK_SIZE = 30

    def __init__(self, game):
        super().__init__(game)
        self.resize()

        self.grid = ClassicGrid(self.BLOCK_SIZE)
        self.player = Player(self.grid, ARROW_KEYS)
        self.background = ParallaxBackground('background',
                                             (self.SCREEN_HEIGHT,
                                              self.SCREEN_HEIGHT))

        self.hud = HUD()
        self.hud.add_item(label='message',
                          position=(self.SCREEN_WIDTH // 2 + 65,
                                    self.SCREEN_HEIGHT // 2),
                          size=70,
                          color=(255, 255, 255),
                          value='GAME OVER',
                          visible=False,
                          center_x=True)

        self.game_over = False

    def resize(self):
        display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))

    def tick(self, dt):
        self.background.tick(dt)
        # The scene isn't removed immediately when the game over flag is set.
        # It is removed at the next tick because we want to render the hud
        # message before removing this scene. For this reason we check the if
        # the game is over at the start of the tick method.
        if self.player.is_game_over():
            self.game.remove_scene()
            return

        self.player.tick(dt)

    def event(self, event):
        self.player.event(event)

    def render(self, surface):
        if self.player.is_game_over():
            self.hud.toggle_visibility('message')
            self.game.set_delay(2500)

        self.background.render(surface, ANIMATED_BACKGROUND)
        self.player.render(surface)
        self.hud.render(surface)

from pygame import display, KEYDOWN, K_UP, K_DOWN, K_RETURN

from config import ANIMATED_BACKGROUND
from hud import HUD
from parallax import ParallaxBackground
from scenes.base import Scene
from scenes.coop import Coop
from scenes.multiplayer import Multiplayer
from scenes.singleplayer import Singleplayer
from utilities import play_sound


class Menu(Scene):
    MARGIN_Y = 50  # vertical space between options

    TITLE_SIZE = 58
    OPTION_SIZE = 46

    TEXT_COLOR = (255, 255, 255)
    SELECTED_COLOR = (0, 255, 255)

    def __init__(self, game, title):
        super().__init__(game)
        screen_res = (game.SCREEN_WIDTH, game.SCREEN_HEIGHT)
        self.background = ParallaxBackground('background', screen_res)

        self.options_num = 0
        self.current_option = 0
        self.scenes = []
        menu_position = (screen_res[0] / 2, screen_res[1] / 4)
        self.option_position = [menu_position[0],
                                menu_position[1] + 2 * Menu.MARGIN_Y]
        self.hud = HUD()
        self.hud.add_item(label='title',
                          position=menu_position,
                          size=Menu.TITLE_SIZE,
                          color=Menu.TEXT_COLOR,
                          value=title,
                          center_x=True)

    @classmethod
    def new_menu(cls, game, title):
        menu = Menu(game, title)
        menu.add_option('SINGLE PLAYER', Singleplayer)
        menu.add_option('MULTIPLAYER', Multiplayer)
        menu.add_option('CO-OP', Coop)
        menu.add_option('QUIT')

        return menu

    def add_option(self, option, scene=None):
        """
        Add an option to the menu and the scene that will be created
        when that option is selected. If scene is None the game quits.
        :param (str) option:
        :param (Scene class) scene: The scene object is created when the option
        is selected.
        """
        self.hud.add_item(label=self.options_num,
                          position=self.option_position,
                          size=Menu.OPTION_SIZE,
                          color=Menu.TEXT_COLOR,
                          value=option,
                          center_x=True)
        self.scenes.append(scene)

        # update the y position for the next option
        self.option_position[1] += Menu.MARGIN_Y
        self.options_num += 1

    def tick(self, dt):
        self.background.tick(dt)

    def resize(self):
        display.set_mode((self.game.SCREEN_WIDTH, self.game.SCREEN_HEIGHT))

    def event(self, event):
        """
        Check keyboard input.
        :param (pygame event) event:
        """
        previous_option = self.current_option
        if self.options_num:
            if event.type == KEYDOWN and event.key == K_UP:         # move up
                play_sound('menu_move')
                if self.current_option == 0:
                    self.current_option = self.options_num - 1
                else:
                    self.current_option -= 1
            elif event.type == KEYDOWN and event.key == K_DOWN:     # move down
                play_sound('menu_move')
                self.current_option = \
                    (self.current_option + 1) % self.options_num
            elif event.type == KEYDOWN and event.key == K_RETURN:   # enter
                play_sound('select')
                scene = self.scenes[self.current_option]
                if scene is None:
                    exit()
                else:
                    self.game.add_scene(scene(self.game))

            self.hud.set_color(previous_option, Menu.TEXT_COLOR)
            self.hud.set_color(self.current_option, Menu.SELECTED_COLOR)

    def render(self, surface):
        self.background.render(surface, ANIMATED_BACKGROUND)
        self.hud.render(surface)

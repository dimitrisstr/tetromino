class Scene:
    def __init__(self, game):
        """
        :param (Game) game:
        """
        self.game = game

    def resize(self):
        """
        Change the size of the window.
        """
        pass

    def tick(self, dt):
        """
        Tick the scene.
        :param (float) dt: time delta
        """
        pass

    def event(self, event):
        """
        Handle an event.
        :param (pygame event) event:
        """
        pass

    def render(self, surface):
        """
        Render the scene.
        """
        pass

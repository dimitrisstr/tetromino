import random

from tetromino import TetrominoType


class BagRandomizer:
    def __init__(self):
        self.current_item = 0
        self.items = list(TetrominoType)
        random.shuffle(self.items)

    def get(self):
        if self.current_item == len(self.items):
            self.current_item = 0
            self.shuffle()

        item = self.items[self.current_item]
        self.current_item += 1

        return item

    def shuffle(self):
        random.shuffle(self.items)

    def reset(self):
        self.current_item = 0
        self.shuffle()

from enum import Enum

from utilities import load_blocks


class TetrominoType(Enum):
    I = 0
    O = 1
    T = 2
    J = 3
    L = 4
    S = 5
    Z = 6


class Tetromino:
    # I tetromino
    i_tetromino_angle_0 = ["....",
                           "####",
                           "....",
                           "...."]

    # rotate 90 degrees cw (clockwise)
    i_tetromino_angle_90 = [".#..",
                            ".#..",
                            ".#..",
                            ".#.."]

    i_tetromino = (i_tetromino_angle_0, i_tetromino_angle_90,
                   i_tetromino_angle_0, i_tetromino_angle_90)

    # O tetromino
    o_tetromino_angle_0 = ["##..",
                           "##..",
                           "....",
                           "...."]

    o_tetromino = (o_tetromino_angle_0, o_tetromino_angle_0,
                   o_tetromino_angle_0, o_tetromino_angle_0)

    # T tetromino
    t_tetromino_angle_0 = [".#..",
                           "###.",
                           "....",
                           "...."]

    # rotate 90 degrees cw
    t_tetromino_angle_90 = [".#..",
                            ".##.",
                            ".#..",
                            "...."]

    # rotate 180 degrees cw
    t_tetromino_angle_180 = ["....",
                             "###.",
                             ".#..",
                             "...."]

    # rotate 270 degrees cw
    t_tetromino_angle_270 = [".#..",
                             "##..",
                             ".#..",
                             "...."]

    t_tetromino = (t_tetromino_angle_0, t_tetromino_angle_90,
                   t_tetromino_angle_180, t_tetromino_angle_270)

    # J tetromino
    j_tetromino_angle_0 = ["#...",
                           "###.",
                           "....",
                           "...."]

    # rotate 90 degrees cw
    j_tetromino_angle_90 = [".##.",
                            ".#..",
                            ".#..",
                            "...."]

    # rotate 180 degrees cw
    j_tetromino_angle_180 = ["....",
                             "###.",
                             "..#.",
                             "...."]

    # rotate 270 degrees cw
    j_tetromino_angle_270 = [".#..",
                             ".#..",
                             "##..",
                             "...."]

    j_tetromino = (j_tetromino_angle_0, j_tetromino_angle_90,
                   j_tetromino_angle_180, j_tetromino_angle_270)

    # L tetromino cw
    l_tetromino_angle_0 = ["..#.",
                           "###.",
                           "....",
                           "...."]

    # rotate 90 degrees cw
    l_tetromino_angle_90 = [".#..",
                            ".#..",
                            ".##.",
                            "...."]

    # rotate 180 degrees cw
    l_tetromino_angle_180 = ["....",
                             "###.",
                             "#...",
                             "...."]

    # rotate 270 degrees cw
    l_tetromino_angle_270 = ["##..",
                             ".#..",
                             ".#..",
                             "...."]

    l_tetromino = (l_tetromino_angle_0, l_tetromino_angle_90,
                   l_tetromino_angle_180, l_tetromino_angle_270)

    # S tetromino
    s_tetromino_angle_0 = [".##.",
                           "##..",
                           "....",
                           "...."]

    # rotate 90 degrees cw
    s_tetromino_angle_90 = [".#..",
                            ".##.",
                            "..#.",
                            "...."]

    s_tetromino = (s_tetromino_angle_0, s_tetromino_angle_90,
                   s_tetromino_angle_0, s_tetromino_angle_90)

    # Z tetromino
    z_tetromino_angle_0 = ["##..",
                           ".##.",
                           "....",
                           "...."]

    # rotate 90 degrees cw
    z_tetromino_angle_90 = ["..#.",
                            ".##.",
                            ".#..",
                            "...."]

    z_tetromino = (z_tetromino_angle_0, z_tetromino_angle_90,
                   z_tetromino_angle_0, z_tetromino_angle_90)

    TETROMINOES = (i_tetromino, o_tetromino, t_tetromino, j_tetromino,
                   l_tetromino, s_tetromino, z_tetromino)
    FAST_MOVE_TIME = 0.03  # seconds
    """ The size of the glow block is GLOW_SCALE * block size. """
    GLOW_SCALE = 2

    def __init__(self, tetromino_type, block_size, position, move_time):
        """
        :param (TetrominoType) tetromino_type:
        :param (int) block_size:
        :param (tuple) position: (x, y) Coordinates.
        :param (float) move_time: Tetromino moves every move_time seconds.
        """
        self.tetromino_type = tetromino_type
        self.tetromino_frames = Tetromino.TETROMINOES[tetromino_type.value]
        self.current_angle = 0

        self.block_size = block_size
        # load sprites
        self.blocks = load_blocks('blocks', block_size)
        self.blocks_glow = load_blocks('blocks_glow',
                                       int(block_size * self.GLOW_SCALE))

        # coordinates of the center block (required for the rotation)
        self.center_coord = list(position)
        # coordinates for each block of the tetromino
        self.blocks_coords = self._build()

        # time in seconds
        self.normal_move_time = move_time
        self.move_time = self.normal_move_time
        self.elapsed_time = 0.0

    def set_coords(self, center_coord):
        self.center_coord = list(center_coord)
        self.blocks_coords = self._build()

    def get_coords(self):
        return self.blocks_coords

    def get_type(self):
        return self.tetromino_type

    def speed_up(self):
        self.move_time = Tetromino.FAST_MOVE_TIME

    def reset_speed(self):
        self.move_time = self.normal_move_time

    def set_speed(self, move_time):
        self.normal_move_time = move_time
        self.move_time = self.normal_move_time

    def render(self, surface):
        """
        Render normal and glow blocks.
        :param (Surface) surface:
        """
        offset = abs(self.block_size - self.blocks_glow[0].get_width()) // 2
        for coord in self.blocks_coords:
            surface.blit(self.blocks_glow[self.tetromino_type.value],
                         (coord[0] - offset, coord[1] - offset))
            surface.blit(self.blocks[self.tetromino_type.value], coord)

    def move_up(self):
        """
        Move up all blocks.
        """
        for coord in self.blocks_coords:
            coord[1] -= self.block_size
        self.center_coord[1] -= self.block_size

    def move_down(self, time):
        """
        Move down all blocks.
        """
        self.elapsed_time += time
        if self.elapsed_time >= self.move_time:
            self.elapsed_time = 0
            for coord in self.blocks_coords:
                coord[1] += self.block_size
            self.center_coord[1] += self.block_size

    def move_left(self):
        """
        Move left all blocks.
        """
        self.center_coord[0] -= self.block_size
        for coord in self.blocks_coords:
            coord[0] -= self.block_size

    def move_right(self):
        """
        Move right all blocks.
        """
        self.center_coord[0] += self.block_size
        for coord in self.blocks_coords:
            coord[0] += self.block_size

    def rotate_ccw(self):
        """
        Rotate the tetromino 90 degrees counterclockwise.
        """
        if self.current_angle == 0:
            self.current_angle = 3
        else:
            self.current_angle -= 1

        self.blocks_coords = self._build()

    def rotate_cw(self):
        """
        Rotate the tetromino 90 degrees clockwise.
        """
        self.current_angle = (self.current_angle + 1) % 4
        self.blocks_coords = self._build()

    def _build(self):
        """
        Calculates the coordinates of each block that is part of the tetromino.
        :return: List of coordinates [x, y].
        """
        # the index of the center block is (1, 1), that's why we subtract
        # block size to find the coordinates of the block (0, 0)
        x, y = [self.center_coord[0] - self.block_size,
                self.center_coord[1] - self.block_size]
        tetromino_coords = []

        current_frame = self.tetromino_frames[self.current_angle]
        for line in current_frame:
            for char in line:
                if char == '#':
                    tetromino_coords.append([x, y])
                x += self.block_size
            x = self.center_coord[0] - self.block_size
            y += self.block_size
        return tetromino_coords

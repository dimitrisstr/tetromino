from os import listdir, path
from pygame import Rect

from utilities import load_sprite


class ParallaxBackground:
    def __init__(self, folder, size):
        self.width, self.height = size

        files = listdir(path.join('assets', 'sprites', folder))
        files.sort()
        self.sprites = [load_sprite(path.join(folder, file[:-4]),
                                    size, crop=True)
                        for file in files]

        self.top_rects = [Rect(0, self.height, self.width, 0)
                          for _ in range(len(self.sprites))]
        self.bottom_rects = [sprite.get_rect() for sprite in self.sprites]

        # Move times for each sprite of the parallax background. The
        # sprite is scrolled every x milliseconds. The time in move_times
        # and elapsed_times is measured in milliseconds.
        self.move_times = [i * 0.03 for i in range(len(self.sprites))]
        self.elapsed_times = [0 for _ in range(len(self.sprites))]

    def tick(self, dt):
        for i in range(1, len(self.move_times)):
            self.elapsed_times[i] += dt
            if self.elapsed_times[i] >= self.move_times[i]:
                self.elapsed_times[i] = 0
                self.scroll(i)

    def render(self, surface, animation=True):
        surface.blit(self.sprites[0], (0, 0))
        if animation:
            for i in range(1, len(self.sprites)):
                surface.blit(self.sprites[i], (0, 0), self.top_rects[i])
                surface.blit(self.sprites[i], self.bottom_rects[i])

    def scroll(self, index):
        if self.bottom_rects[index].y >= self.height:  # Reset
            self.top_rects[index].y = self.height
            self.top_rects[index].height = 0
            self.bottom_rects[index].y = 0
        else:                                          # Scroll
            self.bottom_rects[index].y += 1
            self.top_rects[index].y -= 1
            self.top_rects[index].height += 1

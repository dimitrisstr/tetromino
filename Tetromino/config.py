from configparser import ConfigParser
from os import path


config_filename = 'config.ini'

default_config = {
    'General': {
        'fps': 60,
        'display_fps': False,
        'animated_background': True
    },
    'Sound': {
        'mute': False
    }
}

config = ConfigParser()
if path.exists(config_filename):
    config.read(config_filename)
else:
    config.read_dict(default_config)
    with open(config_filename, 'w') as config_file:
        config.write(config_file)

""" General """
FPS = config.getint('General', 'fps')
DISPLAY_FPS = config.getboolean('General', 'display_fps')
ANIMATED_BACKGROUND = config.getboolean('General', 'animated_background')

""" Sound """
MUTE = config.getboolean('Sound', 'mute')

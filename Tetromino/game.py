import pygame as pg

from config import DISPLAY_FPS
from hud import HUD
from scenes.menu import Menu
from scenes.pause import Pause
from utilities import toggle_mute


class Game:
    SCREEN_WIDTH = 640
    SCREEN_HEIGHT = 720

    # key repeat
    DELAY = 250     # milliseconds
    INTERVAL = 50   # milliseconds

    def __init__(self, fps):
        pg.init()
        pg.mixer.init()
        pg.key.set_repeat(self.DELAY, self.INTERVAL)

        title = 'Tetromino'
        pg.display.set_caption(title)
        self.screen = pg.display.set_mode((self.SCREEN_WIDTH,
                                           self.SCREEN_HEIGHT))

        self.clock = pg.time.Clock()
        self.fps = fps
        self.dt = 1.0 / self.fps
        self.accumulator = 0.0
        self.delay = 0  # milliseconds

        self.scenes = [Menu.new_menu(self, title)]
        self.hud = HUD()

        self.hud.add_item(label='fps', position=(0, 0), size=25,
                          color=(255, 255, 255), value='FPS:',
                          visible=DISPLAY_FPS)

    def add_scene(self, scene):
        self.scenes.append(scene)

    def remove_scene(self):
        """
        Remove the last scene if there are more that one scenes. The
        first scene is the menu. Also call the resize method of the new
        active scene because some scenes change the size of the window.
        """
        if len(self.scenes) > 1:
            self.scenes.pop()
            self.scenes[-1].resize()

    def set_delay(self, milliseconds):
        """
        Set how much time the game will be paused after the rendering phase.
        It doesn't pause the game immediately.
        :param (int) milliseconds:
        """
        self.delay = milliseconds

    def size(self):
        return self.screen.get_size()

    def step(self):
        self._event()

        frame_time = self.clock.tick(self.fps) / 1000.0  # convert to seconds
        self.accumulator += frame_time
        while self.accumulator >= self.dt:
            # game logic
            self._tick(self.dt)
            self.accumulator -= self.dt

        self._render()

        if self.delay:
            pg.time.wait(self.delay)
            self.delay = 0

    def _event(self):
        """
        Input handling.
        """
        for event in pg.event.get():
            # Quit
            if event.type == pg.QUIT:
                exit()
            # Back
            elif event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                self.remove_scene()
            # Pause
            elif event.type == pg.KEYUP and event.key == pg.K_p:
                if isinstance(self.scenes[-1], Pause):
                    self.scenes[-1].event(event)
                elif not isinstance(self.scenes[-1], Menu):
                    self.add_scene(Pause(self))
            # Mute
            elif event.type == pg.KEYUP and event.key == pg.K_m:
                toggle_mute()
            else:
                self.scenes[-1].event(event)

    def _tick(self, dt):
        """
        Game logic.
        """
        self.scenes[-1].tick(dt)

    def _render(self):
        """
        Render active scene.
        """
        self.scenes[-1].render(self.screen)
        self.hud.update_value('fps', f'FPS: {int(self.clock.get_fps())}')
        self.hud.render(self.screen)

        pg.display.flip()

import os
import pygame as pg
from pygame import image, transform, mixer

from config import MUTE

_GFX_CACHE = {}
_SFX_CACHE = {}

# (Left, Right, Rotate Clockwise, Rotate Counterclockwise, Drop, Speed Up)
ARROW_KEYS = (pg.K_LEFT, pg.K_RIGHT, pg.K_UP, pg.K_DOWN, pg.K_SPACE, pg.K_RSHIFT)
LETTER_KEYS = (pg.K_a, pg.K_d, pg.K_w, pg.K_s, pg.K_LSHIFT, pg.K_z)


def load_sprite(name, dimensions=None, crop=False):
    if (name, dimensions) not in _GFX_CACHE:
        path = os.path.join('assets', 'sprites', f'{name}.png')
        sprite = image.load(path).convert_alpha()
        if dimensions and not crop:
            sprite = transform.scale(sprite, dimensions)
        elif dimensions and crop:
            sprite = sprite.subsurface(0, 0, *dimensions)

        _GFX_CACHE[name, dimensions] = sprite

    return _GFX_CACHE[name, dimensions]


def load_blocks(name, size):
    key = (name, None)
    if key not in _GFX_CACHE:
        block_list = []
        blocks = load_sprite(name)

        blocks_num = blocks.get_width() // blocks.get_height()
        blocks = transform.scale(blocks, (blocks_num * size, size))
        for i in range(blocks_num):
            block_surface = blocks.subsurface(i * size, 0, size, size)
            block_list.append(block_surface)

        _GFX_CACHE[key] = block_list

    return _GFX_CACHE[key]


def play_sound(name):
    if MUTE:
        return

    if name not in _SFX_CACHE:
        sound = mixer.Sound(os.path.join('assets', 'sounds', f'{name}.wav'))
        _SFX_CACHE[name] = sound

    _SFX_CACHE[name].play()


def toggle_mute():
    global MUTE
    MUTE = not MUTE


def write(font, message, color):
    text = font.render(str(message), True, color)
    text = text.convert_alpha()
    return text


def center_surface(surface, position):
    return position[0] - surface.get_width() // 2,\
           position[1] - surface.get_height() // 2
